#!/bin/bash
A="{{ B }}"
hostname="123.33.0..1"

javapath="/home/ec2-user/wm"

cpppath="/home/ec2-user/wm/cbs"

mkdir -p /tmp/JAVA_`date +%F`_`hostname -f`
mkdir -p /tmp/CPP_`date +%F`_`hostname -f`
find $javapath -type f -newermt "$A" -exec cp {} /tmp/JAVA_`date +%F`_`hostname -f` \;
find $cpppath -type f -newermt "$A" -exec cp {} /tmp/CPP_`date +%F`_`hostname -f` \;
#zip -rm JAVA_`date +%F`_`hostname -f`.zip JAVA_`date +%F`_`hostname -f`
#zip -rm CPP_`date +%F`_`hostname -f`.zip CPP_`date +%F`_`hostname -f`