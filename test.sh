#! /bin/bash

export MIP_HOME=/home/ec2-user/mip/
export MDA_HOME=/home/ec2-user/mda/
export WM_HOME=/home/ec2-user/wm/
export DASH_HOME=/home/ec2-user/dash/

export FS=/home

#Checking if this script is being executed as ROOT. For maintaining proper directory structure, this script must be run from a root user.
#if [ $EUID != 0 ]
#then
#  echo "Please run this script as root so as to see all details! Better run with sudo."
#  exit 1
#fi
#Declaring variables
#set -x
os_name=`uname -r | awk {'print$1'} | cut -f2 -d'-'`
upt=`uptime`
ip_add=`ifconfig | grep "inet" | head -1 | awk {'print$2'}`
num_proc=`ps -ef | wc -l`

total_root_size=`df -h / | tail -1 | awk '{print$2}'`
root_size_current=`df -h / | tail -1 | awk '{print $4 " -  " $5}'`
root_fs_pc=`df -h / | tail -1 | awk '{print $(NF-1)}' | grep -o '[0-9]\+'`

total_app_size=`df -h $FS | tail -1 | awk '{print$2}'`
app_size_current=`df -h $FS | tail -1 | awk '{print $4 " -  " $5}'`
app_fs_pc=`df -h $FS | tail -1 | awk '{print $(NF-1)}' | grep -o '[0-9]\+'`

#load_avg=`uptime | cut -f5 -d':'`
load_avg=`cat /proc/loadavg  | awk {'print$1,$2,$3'}`

ram_usage=`free -g | head -2 | tail -1 | awk {'print$3'}`G
ram_total=`free -g | head -2 | tail -1 | awk {'print$2'}`G

numberofcpu=`lscpu | egrep 'Model name|Socket|Thread|NUMA|CPU\(s\)' | head -1 | awk '{print $2}'`

hostname=`hostname -f`
disk=`df -h / |tail -1|awk '{print $(NF-1)}' | grep -o '[0-9]\+'`
disk_perc=`df -h / |tail -1|awk '{print $(NF-1)}'`
MIP_HEAP=$(for i in `find $MIP_HOME -type f -name mip.properties`;do echo $i;cat $i|grep -E 'java.min.heap.size|java.max.heap.size'|grep -v '#';done)
MDA_HEAP=$(for i in `find $MDA_HOME -type f -name env.properties`;do echo $i;cat $i|grep -E 'java.min.heap.size|java.max.heap.size'|grep -v '#';done)
WM_HEAP=$(for i in `find $WM_HOME -type f -name env.properties`;do echo $i;cat $i|grep -E 'java.min.heap.size|java.max.heap.size'|grep -v '#';done)
DASH_HEAP=$(for i in `find $DASH_HOME -type f -name env.properties`;do echo $i;cat $i|grep -E 'java.min.heap.size|java.max.heap.size'|grep -v '#';done)

snapshotcollector=`find $WM_HOME -type f -name WSC.sh`

#nmon="`crontab -l | grep -i "nmon.sh"`"
nmon=$(crontab -l | grep "nmon.sh")
#nmon_prec=$(crontab -l | grep "$nmon")

#Creating a directory if it doesn't exist to store reports first, for easy maintenance.
if [ ! -d /tmp/health_reports ]
then
  mkdir /tmp/health_reports
fi
html="/tmp/health_reports/Server-Health-Report.html"
email_add="vprasadk@manh.com"
#for i in `ls /home`; do sudo du -sh /home/$i/* | sort -nr | grep G; done > /tmp/dir.txt
#Generating HTML file
echo "<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">" >> $html
echo "<html>" >> $html
echo "<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css">" >> $html
echo "<body>" >> $html
echo "<fieldset>" >> $html
echo "<center>" >> $html
echo "<h2>Server Health Check" >> $html
echo "</center>" >> $html
echo "</fieldset>" >> $html
echo "<br>" >> $html
echo "<center>" >> $html
echo "<h2>OS Details : </h2>" >> $html
echo "<table class="pure-table" style=\"border: 1px solid black ;\">" >> $html
echo "<thead>" >> $html
echo "<tr>" >> $html
echo "<th style="text-align: center">OS Name</th>" >> $html
echo "<th style="text-align: center">IP Address</th>" >> $html
echo "<th style="text-align: center">Hostname</th>" >> $html
echo "<th style="text-align: center">Uptime</th>" >> $html

echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tbody>" >> $html
echo "<tr>" >> $html
echo "<td>$os_name</td>" >> $html
echo "<td>$ip_add</td>" >> $html
echo "<td>$hostname</td>" >> $html
echo "<td>$upt</td>" >> $html



echo "</tr>" >> $html
echo "</tbody>" >> $html
echo "</table>" >> $html


echo "<center>" >> $html
echo "<h2>HEAP MEMORY </h2>" >> $html
echo "<table class="pure-table" style=\"border: 1px solid black ;\">" >> $html
echo "<thead>" >> $html
echo "<tr>" >> $html
echo "<"th">MIP :</th>" >> $html
echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tbody>" >> $html
echo "<tr>" >> $html
echo "<td>$MIP_HEAP</td>" >> $html
echo "</tr>" >> $html
echo "<thead>" >> $html
echo "<tr>" >> $html
echo "<"th">MDA :</th>" >> $html
echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tr>" >> $html
echo "<td>$MDA_HEAP</td>" >> $html
echo "</tr>" >> $html
echo "<thead>" >> $html
echo "<tr>" >> $html
echo "<"th">WM :</th>" >> $html
echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tr>" >> $html
echo "<td>$WM_HEAP</td>" >> $html
echo "</tr>" >> $html
echo "<thead>" >> $html
echo "<tr>" >> $html
echo "<"th">DASH :</th>" >> $html
echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tr>" >> $html
echo "<td>$DASH_HEAP</td>" >> $html
echo "</tr>" >> $html
echo "</tbody>" >> $html
echo "</table>" >> $html



echo "<h2>Resources Utilization : </h2>" >> $html
echo "<br>" >> $html
echo "<table class="pure-table" style=\"border: 1px solid black ;\">" >> $html
echo "<thead>" >> $html
echo "<tr style=\"border: 1px solid black ;\">" >> $html
echo "<th>No of Processes</th>" >> $html
echo "<th>Load Average</th>" >> $html
echo "<th>Total Size of Root FS</th>" >> $html
echo "<th>Root current Usage</th>" >> $html
echo "<th>Total Size of App FS</th>" >> $html
echo "<th>App current Usage</th>" >> $html
echo "<th>Total RAM(in GB)</th>" >> $html
echo "<th>Used RAM(in GB)</th>" >> $html
echo "<th>Number of CPU</th>" >> $html
echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tbody>" >> $html
echo "<tr style=\"border: 1px solid black ;\">" >> $html
echo "<td><center>$num_proc</center></td>" >> $html
echo "<td><center>$load_avg</center></td>" >> $html
echo "<td><center>$total_root_size</center></td>" >> $html

if [ $root_fs_pc -ge 70 ]
        then
                echo "<td style="background-color:red"><center>$root_size_current</center></td>" >> $html
        else
                echo "<td style="background-color:#8CFA07"><center>$root_size_current</center></td>" >> $html
fi

echo "<td><center>$total_app_size</center></td>" >> $html

if [ $app_fs_pc -ge 70 ]
        then
                echo "<td style="background-color:red"><center>$app_size_current</center></td>" >> $html
        else
                echo "<td style="background-color:#8CFA07"><center>$app_size_current</center></td>" >> $html
fi

echo "<td><center>$ram_total</center></td>" >> $html
echo "<td><center>$ram_usage</center></td>" >> $html
echo "<td><center>$numberofcpu</center></td>" >> $html
echo "</tr>" >> $html
echo "</tbody>" >> $html
echo "</table>" >> $html

echo "<h2>NMON & SNAPSHOT Validation :</h2>" >> $html
echo "<br>" >> $html
echo "<table class="pure-table" style=\"border: 1px solid black ;\">" >> $html
echo "<thead>" >> $html
echo "<tr style=\"border: 1px solid black ;\">" >> $html
echo "<th>Utility</th>" >> $html
echo "<th>Available</th>" >> $html
echo "<th>Path</th>" >> $html
echo "</tr>" >> $html
echo "</thead>" >> $html
echo "<tbody>" >> $html
echo "<tr style=\"border: 1px solid black ;\">" >> $html
echo "<td><center>NMON</center></td>" >> $html

if [ ! -z "$nmon" ]
then
    echo "<td><center>Yes</center></td>" >> $html
else
    echo "<td><center>No</center></td>" >> $html
fi

#echo "<td><center>Yes</center></td>" >> $html
echo "<td><center>$nmon</center></td>" >> $html
echo "</tr>" >> $html
echo "<tr style=\"border: 1px solid black ;\">" >> $html
echo "<td><center>Snapshot Collector</center></td>" >> $html

if [ ! -z $snapshotcollector ]
then
    echo "<td><center>Yes</center></td>" >> $html
else
    echo "<td><center>No</center></td>" >> $html
fi
#echo "<td><center>Yes</center></td>" >> $html
echo "<td><center>$snapshotcollector</center></td>" >> $html
echo "</tr>" >> $html
echo "</tbody>" >> $html
echo "</table>" >> $html


echo "</table>" >> $html
echo "</body>" >> $html
echo "</html>" >> $html
echo "Report has been generated in /tmp/health_reports with file-name = $html. Report has also been sent to $email_add."
#Sending Email to the user
#cat $html | mail -s "`hostname` - Daily System Health Report" -a "MIME-Version: 1.0" -a "Content-Type: text/html" -a "From: Vishnu Prasad K <root@vprasadk.com>" $email_add